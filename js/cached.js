/**
 * Start
 */

var TestApplication = {};

window.onload = function()
{
	TestApplication.element = document.getElementById('test_container');
	TestApplication.element.innerHTML = '<p>start.js has loaded, which requires nothing</p>';

	TestApplication.First();
	TestApplication.Second();
	TestApplication.Third();
	TestApplication.Caca();
};

// EOF



// requires start

TestApplication.First = function() {

	TestApplication.element.innerHTML += '<p>first.js has loaded, which requires "start"</p>';

};

// EOF



// requires first

TestApplication.Third = function() {

	TestApplication.element.innerHTML += '<p>modules/module_two.js has loaded, which requires "first"</p>';

};

// EOF



// requires modules/module_two

TestApplication.Caca = function() {

	TestApplication.element.innerHTML += '<p>modules/module_caca.js has loaded, which requires "modules/module_two"</p>';

};

// EOF



// requires first

TestApplication.Second = function() {

	TestApplication.element.innerHTML += '<p>modules/module_one.js has loaded, which requires "first"</p>';

};

// EOF


(function()
{
	// requires modules/module_two

	var log_this = '';

	log_this += 'src/subfolder/subsubfolder/rogue_file.js here!\n';
	log_this += 'Rogue file with no logical file tree indication of its dependencies has loaded.\n';
	log_this += '(requires modules/module_two)';

	console.log(log_this);
})();

// EOF


