/**
 * Start
 */

var TestApplication = {};

window.onload = function()
{
	TestApplication.element = document.getElementById('test_container');
	TestApplication.element.innerHTML = '<p>start.js has loaded, which requires nothing</p>';

	TestApplication.First();
	TestApplication.Second();
	TestApplication.Third();
	TestApplication.Caca();
};

// EOF